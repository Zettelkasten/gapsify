<?php

// connect to DB
try {
	// load MySQL credentials from DOCUMENT_ROOT/../gapsify.json (i.e. var/www/gapsify.json)
	$config = json_decode(file_get_contents(realpath($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . '..') . DIRECTORY_SEPARATOR . 'gapsify.json'), true);
	
	$db = new PDO('mysql:dbname='.$config['database'].';host='.$config['host'].';charset=utf8', $config['username'], $config['password']);
	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(PDOException $e) {
	die('Connection failed: '.$e->getMessage());
}

/*
 * What to display, either:
 *  - 'create' to create a new Gaps
 *  - 'show' to show an existing Gaps
 */
$mode;

if (isset($_GET['create'])) {
	$mode = 'create';
} else if(isset($_GET['show'])) {
	$mode = 'show';
	
	if(!isset($_GET['id'])) {
		die('Missing gaps id');
	}
	$id = $_GET['id'];
	
	$sth = $db->prepare('SELECT Id, Time, Text, Title FROM `gaps` WHERE Id = :id;');
	$sth->bindParam(':id', $id);
	if (!$sth->execute()) {
		die('Failed to execute query');
	}

	$result = $sth->fetch();

	if ($result === false) {
		die('Unknown id');
	}
} else {
	die('Unknow mode.');
}
?>
<html>
	<head>
		<title>Gapsify</title>

		<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

		<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

		<?php if ($config['useLocalLibs']) { ?>
		<link rel="stylesheet" href="libs/materialize.min.css">
		<script src="libs/jquery.min.js"></script>
		<script src="libs/jquery-ui.min.js"></script>
		<script src="libs/materialize.min.js"></script>
		<script src="libs/jquery.ui.touch-punch.min.js"></script>
		<script src="libs/xregexp-all-min.js"></script>
		<?php } else { ?>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/css/materialize.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/js/materialize.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.2/jquery.ui.touch-punch.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/xregexp/2.0.0/xregexp-all-min.js"></script>
		<?php } ?>

		<link rel="stylesheet" href="style.css">
		
		<script>
		var mode = <?php echo json_encode($mode); ?>;
		<?php if ($mode == 'show') { ?> var gapsData = <?php echo json_encode(array('id' => $result['Id'], 'text' => $result['Text'], 'time' => $result['Time'], 'title' => $result['Title'])); ?>; <?php } ?>
		</script>
		<script src="gapsify.js"></script>
		<script src="script.js"></script>
	</head>
	<body>
		<div class="container">
			<h3>GAPSify</h3>
			<!-- GAPS CREATE -->
			<?php if ($mode == 'create') { ?>
			<div id="gaps-create" class="card indigo lighten-1">
				<div class="card-content white-text">
					<textarea id="text" class="flow-text indigo lighten-2" placeholder="Insert the text you want to use and click 'Gapsify'."></textarea>
				</div>
				<div class="card-action" align="right">
					<a id="gaps-submit" href="#">Gapsify!</a>
				</div>
			</div>
			<?php } ?>
			<!-- GAPS SHOW -->
			<h4 class="card-label gaps-preview" style="display: none">Preview</h4>
			<div id="gaps-show" class="card blue-grey darken-1 gaps-preview" style="display: <?php echo $mode == 'show' ? 'block' : 'none'; ?>">
				<div class="card-content white-text">
					<span class="card-title"><?php if ($mode == 'show') { echo $result['Title']; } ?></span>
					<div id="gaps-solve">
						<!-- preloader start -->
						<div id="gaps-solve-preloader" class="center-align">
							<div class="preloader-wrapper active">
								<div class="spinner-layer spinner-red-only">
									<div class="circle-clipper left">
										<div class="circle"></div>
									</div><div class="gap-patch">
										<div class="circle"></div>
									</div><div class="circle-clipper right">
										<div class="circle"></div>
									</div>
								</div>
							</div>
						</div>
						<!-- preloader end -->
					</div>
					<div id="gaps-results" class="card" style="display: none">
						<div class="progress">
							<div class="determinate red" id="gaps-progress-wrong" style="width: 0"></div>
							<div class="determinate green" id="gaps-progress-correct" style="width: 0"></div>
						</div>
						<div class="card-content black-text">
							You filled out <em id="gaps-correct"></em> of <em id="gaps-total"></em> gaps correctly!
						</div>
					</div>
				</div>
				<?php if ($mode == 'show') { ?>
				<div class="card-action" align="right">
					<!-- <a id="gaps-check" href="#">Check results</a> -->
					<a id="gaps-create" href="/">Create your own</a>
				</div>
				<?php } ?>
			</div>
			<!-- GAPS CREATE CONFIRM -->
			<?php if ($mode == 'create') { ?>
			<div id="gaps-create-confirm" class="card indigo lighten-1 gaps-preview" style="display: none">
				<div class="card-content white-text">
					Happy with your Gaps? Put it online!
				</div>
				<div class="card-action" align="right">
					<a id="gaps-publish" href="#">Publish!</a>
				</div>
			</div>
			<?php } ?>
		</div>
	</body>
</html>
