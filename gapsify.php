<?php

// PHP Gapsify implementation
function gapsify($raw) {
	$regex = '/(\\p{L}+)/u';
	global $tokenId;
	$tokenId = 0;
	return preg_replace_callback($regex, function ($tokenArr) {
		$token = $tokenArr[0];
		global $tokenId;
		if ($tokenId % 2 == 0) {
			$gapLength = ceil(strlen($token) / 2);
			$newToken = makeToken($token, $gapLength);
		} else {
			$newToken = $token;
		}
		$tokenId++;
		return $newToken;
	}, $raw);
}

function makeToken($content, $gapLength) {
	$el = '<span class="token">' . substr($content, 0, strlen($content) - $gapLength) . makeGap($gapLength, substr($content, strlen($content) - $gapLength)) . '</span>';
	return $el;
}

$gapId = 0;

function makeGap($length, $value = '') {
	if ($length > 0) {
		$placeholder = '';
		while (strlen($placeholder) < $length) {
			$placeholder .= '_';
		}
		global $gapId;
		$el = '<span class="gap"><span class="gap-placeholder" z-index="-1">' . $placeholder . '</span><input type="text" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" maxlength="' . $length . '" data-gap="' . $gapId . '" id="gap-' . $gapId . '" class="gap-content" value="' . $value . '"/></span>';
		$gapId++;
		return $el;
	} else {
		return '';
	}
}

?>
