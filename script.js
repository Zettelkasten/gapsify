$(document).ready(function() {
	$('#gaps-submit').click(function () {
		loadGaps({
			'text': $('#text').val()
		});
		
		$('.gaps-preview').show();
	});
	
	function loadGaps(data) {
		$('#gaps-solve').gapsify(data.text);
		$('#gaps-results').show();
		if (showResults) {
			updateResult();
		}
	}
	
	$("#gaps-solve").on('keydown', '.gap-content', function(event) {
		// Allow controls such as backspace
		var arr = [8,9,16,17,20,35,36,37,38,39,40,45,46,191];
		
		// Allow letters
		for(var i = 65; i <= 90; i++){
			arr.push(i);
		}

		// Prevent default if not in array
		if(jQuery.inArray(event.which, arr) === -1){
			event.preventDefault();
			console.log('Cancel that! (' + event.which + ')');
		}
	});
	$('#gaps-solve').on('keyup', '.gap-content', function(event) {
		// Allow controls such as backspace
		var arr = [8,9,16,17,20,35,36,37,38,39,40,45,46,191];
		
		console.log(event);
		
		var el = $(this);
		if (el.val().length == el.attr('maxlength') && jQuery.inArray(event.which, arr) === -1) {
			// change focus to next element
			var gapId = el.data('gap') + 1;
			$('#gap-' + gapId).focus();
		}
		var gap = gaps[$(this).data('gap')];
		
		if (showResults) {
			gap.check();
			updateResult();
		}
	});
	$('#gaps-solve').on('focus', '.gap-content', function() {
		var el = $(this);
		el.siblings('.gap-placeholder').addClass('selected');
	});
	$('#gaps-solve').on('focusout', '.gap-content', function() {
		var el = $(this);
		el.siblings('.gap-placeholder').removeClass('selected');
	});
	
	if (typeof gapsData !== 'undefined') {
		$('#gaps-solve').gapsify(gapsData['text']);
		$('#gaps-results').show();
	}
	
	$('#gaps-check').click(function () {
		for (var gapId = 0; gapId < gaps.length; gapId++) {
			gaps[gapId].check();
		}
		updateResult();
		showResults = true;
	});
	if (showResults) {
		updateResult();
	}
	
	function updateResult() {
		var correct = 0;
		var filled = 0;
		var total = gaps.length;
		for (var gapId = 0; gapId < gaps.length; gapId++) {
			var gap = gaps[gapId];
			correct += gap.correct ? 1 : 0;
			filled += gap.filled ? 1 : 0;
		}
		console.log(correct + '/' + total + ' are correct.');
		$('#gaps-progress-wrong').width(filled / total * 100 + '%');
		$('#gaps-progress-correct').width(correct / total * 100 + '%');
		$('#gaps-correct').html(correct);
		$('#gaps-total').html(total);
	}
	
	$('#gaps-publish').click(function() {
		var raw = $('#text').val();
		$.ajax({
			url: '/publish.php',
			method: 'POST',
			data: { text: raw },
			success: function (data) {
				console.log('Did it!');
				console.log(data);
				window.location.href = '/' + data.id;
			}
		});
	});
});
