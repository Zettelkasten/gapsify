<?php
// from http://php.net/manual/de/function.base64-encode.php#103849
function base64url_encode($data) { 
  return rtrim(strtr(base64_encode($data), '+/', '-_'), '='); 
} 

function base64url_decode($data) { 
  return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT)); 
} 

header('Content-Type: application/json; charset=UTF-8');

try {
	// load MySQL credentials from DOCUMENT_ROOT/../gapsify.json (i.e. var/www/gapsify.json)
	$config = json_decode(file_get_contents(realpath($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . '..') . DIRECTORY_SEPARATOR . 'gapsify.json'), true);
	
	$db = new PDO('mysql:dbname='.$config['database'].';host='.$config['host'].';charset=utf8', $config['username'], $config['password']);
	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(PDOException $e) {
	die(json_encode(array('error' => 'Connection failed: '.$e->getMessage())));
}

// the length and all usable characters of an id
$ID_LENGTH = 6;

if (!isset($_POST['text'])) {
	die(json_encode(array('error' => 'Missing POST argument text')));
}
$text = $_POST['text'];

if (!isset($_POST['title'])) {
	$title = null;
} else {
	$title = $_POST['title'];
}

// insert into database with newly generated id until we find an id that hasn't been used yet (should be okay with first try usually)
do {
	$id = substr(base64url_encode(sha1(mt_rand())), rand(0, 54 - $ID_LENGTH), $ID_LENGTH);

	$sth = $db->prepare('INSERT IGNORE INTO `gaps` SET Id = :id, Text = :text, Title = :title;');
	$sth->bindParam(':id', $id);
	$sth->bindParam(':text', $text);
	$sth->bindParam(':title', $title);
	$sth->execute();
} while ($sth->rowCount() != 1);

die(json_encode(array('id' => $id)));
?>