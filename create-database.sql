CREATE TABLE `gaps` (
  `Id` char(6) COLLATE utf8_bin NOT NULL,
  `Time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Text` text COLLATE utf8_bin NOT NULL,
  `Title` varchar(256) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
